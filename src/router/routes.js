export default [
    {
        path: '/',
        component: () =>
            import ('layouts/default/default.vue'),
        children: [
            {
                path: '',
                component: () =>
                    import ('pages/index')
            }
    ]
  },
    {
        path: '/dashboard',
        component: () =>
            import ('layouts/default/default.vue'),
        children: [
            {
                path: '',
                name: 'dashboard',
                component: () =>
                    import ('pages/index')
            }
    ]
  },


    {
        path: '/media',
        component: () =>
            import ('layouts/default/default.vue'),
        children: [
            {
                path: '',
                name: 'media',
                component: () =>
                    import ('pages/media/media')
            }
    ]

  },
    {
        path: '/users',
        component: () =>
            import ('layouts/default/default.vue'),
        children: [
            {
                path: '',
                name: 'users',
                component: () =>
                    import ('pages/users/users')
            }
    ]

  },
    {
        path: '/page/pages',
        component: () =>
            import ('layouts/default/default.vue'),
        children: [
            {
                path: '',
                name: 'pages.index',
                component: () =>
                    import ('pages/page/pages')
            },
            {
                path: 'create',
                name: 'pages.create',
                component: () =>
                    import ('pages/page/pagesCreate')
            },
    ]

  },



    { // Always leave this as last one
        path: '*',
        component: () =>
            import ('pages/404')
  }
]
