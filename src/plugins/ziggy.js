import {
    Ziggy
} from '@/src/router/ziggy';
import route from '@/src/router/route';

window.Ziggy = Ziggy;

export default ({
    Vue
}) => {
    Vue.prototype.$zroute = route;
}
